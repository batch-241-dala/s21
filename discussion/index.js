// ARRAYS
/*
- An array in programming is simply a list of data.
- They are declared using square brackets [], also known as "Array Literals"
- They are usually used to store numerous amounts of data to manipulate in order to perform a number of tasks.
- Arrays provide a number of functions(or methods in other words) that help in achieving this.
- The main difference of an array to an object is that it contains information in a form of "list", unlike objects that uses properties.
- Syntax:
	let/const arrayName = [elementA, elementB, elementC, etc...]
*/

let studentNumberA = '2023-1923';
let studentNumberB = '2023-1924';
let studentNumberC = '2023-1925';
let studentNumberD = '2023-1926';
let studentNumberE = '2023-1927';

// With Array
let studentNumbers = ['2023-1928', '2023-1929', '2023-1930']
console.log(studentNumbers)

// Common examples of an array
let grade = [98.5, 94.3, 99.01, 90.1]
console.log(grade)

let computerBrands = ["Acer", "Asus", "Lenovo", "Neo", "Redfox", "Gateway", "Toshiba", "Fujitsu"]
console.log(computerBrands)

// Array with mixed data types (not recommended)
let mixedArr = (12, 'Asus', null, undefined, {})
console.log(mixedArr)

// Alternative way to write arrays
let myTasks = [
		'drink HTML',
		"eat javascript",
		'inhale CSS',
		"bake sass" 
	];
console.log(myTasks);

// Creating an array with value from variables:
let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "Jakarta";
let cities = [city1, city2, city3];
console.log(cities);

// Length Property
// The .length property allows us to "get" and "set" the total number of items in an array

console.log(myTasks.length);
console.log(cities.length);

let blankArr = [];
console.log(blankArr.length);

// .length property can also be used with strings. Some array methods and properties can be used with strings.
let fullName = "Zybren Carbonilla";
console.log(fullName.length);

// .length property can also set the total numbers of items in an array. Meaning, we can actually delete the last item in the array or shorten the array by updating its length.
myTasks.length = myTasks.length - 1;
console.log(myTasks.length);
console.log(myTasks);

// Using decrementation
cities.length--;
console.log(cities); 


// Adding an item in an array
// If you can shorted the array by setting the length props, we can also lengthen it by adding a number into the length property. But it will be undefined.

let theBeatles = ["John", "Paul", "Ringo", "George"];
theBeatles.length++;
console.log(theBeatles)

// Reading from Arrays
/*
- We can access array elements through the use of array indexes
- In Javascript, the first element is associated with the number 0
- Array indexes actually refer to an address/location in the device's memory and hot the information is stored.
- Syntax:
	arrayName[index]
*/
console.log(grade[0]);
console.log(computerBrands[1]);
// Accessing an array element that does not exist will return "undefined"
console.log(grade[20])

let lakersLegend = ["Kobe", "Shaq", "Lebron", "Magic", "Kareem"]
// Access the second item in the array
console.log(lakersLegend[1]);
// Access the fourth item in the array
console.log(lakersLegend[3]);

// You can save or store array items in another variable
let currentLaker = lakersLegend[2];
console.log(currentLaker);

// You can re-assign array values using the item's indices
console.log("Array before assignment:");
console.log(lakersLegend);
lakersLegend[2] = "Paul Gasol";
console.log("After reassignment: ")
console.log(lakersLegend);

// Accessing the last element of an array
let bullsLegends = ["Jordan", "Pippen", "Rodman", "Rose", "Kukoc"]
let lastElementIndex = bullsLegends.length - 1;
console.log(bullsLegends[lastElementIndex]);

// Adding Items into the Array
// Using indices, we can add items into the array
let newArr = [];
console.log(newArr[0]);

newArr[0] = "Cloud Strife";
console.log(newArr);

console.log(newArr[1])
newArr[1] = "Tifa Lockhart";
console.log(newArr);

newArr[1] = "Arjay Pogi"
console.log(newArr);

newArr[2] = "Dala Arjay";
console.log(newArr);

newArr[newArr.length] = "Last index"
console.log(newArr);

// Looping over an Array
// You can use a for loop to iterate over all items in an array.

for (let index = 0; index < newArr.length; index++) {
	console.log(newArr[index])
}

let numArr = [5, 12, 30, 46, 40];

for (let index = 0; index < numArr.length; index++) {
	if (numArr[index] % 5 === 0) {
		console.log(numArr[index] + " is divisible by 5")
	} else {
		console.log(numArr[index] + " is not divisible by 5");
	}
}

// Multidimensional Arrays
/*
- Multidimensional arrays are useful for storing complex data structure
*/

